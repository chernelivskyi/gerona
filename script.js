// ----#1----//
function avg(array) {

    let s = 0
    for (let i = 0; i < array.length; i++) {
        s += array [i]
    }
    return s / array.length


}

let a = [1, 2, 3]
console.log(avg(a))

// ----#2----//

function avg1(...array) {

    let s = 0
    for (let i = 0; i < array.length; i++) {
        s += array [i]
    }
    return s / array.length
}

console.log(avg1(1, 2, 3, 4, 5, 6, 7, 8, 9))

// ----#3----//
function gerona(a, b, c) {
    const p = (a + b + c) / 2
    return Math.sqrt(p * (p - a) * (p - b) * (p * c))

}

console.log(gerona(2, 3, 4))

// ----#4----//
function subarray(array, count) {
    if (count) {
        let b =[]
        array.push(b)
        subarray(b, count - 1)
    }
}

let b = []
subarray(b, 5)
console.log(b)
